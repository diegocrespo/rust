#[derive(Debug)] // so we can inspect the state in a minute
enum UsState {
    Alabama,
    Alaska,
}

enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
	Coin::Penny => {
	    println!("Lucky Penny!");
	    1
	}
	Coin::Nickel => 5,
	Coin::Dime => 10,
	Coin::Quarter(state) => {
	    println!("State Quarter from {:?}!", state);
	    25
	}
    }
}
#[warn(dead_code)]
fn main() {
    println!("Hello, world!");
    let val = Coin::Penny;
    let _val2 = Coin::Nickel;
    let _val3 = Coin::Dime;
    let val4 = Coin::Quarter(UsState::Alabama);
    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
    value_in_cents(val);
    value_in_cents(val4);
}
fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
	None => None,
	Some(i) => Some(i + 1),
    }
}
