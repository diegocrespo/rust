fn main() {
    // // cannot borrow as mutable
    // let mut s = String::from("hello"); // Heap Allocated and mutable
    // s.push_str(", world!"); // push_str() appends a literal to string

    // println!("{}", s); // This will print `hello, world!`

    // let s1 = String::from("hello");
    // let s2 = s1;

    // println!("{}, world!",s1); // Righteous borrow checker fury
    // let s = String::from("hello"); // s comes into scope

    // println!("s is still valid here. The value is {}", s);

    // takes_ownership(s);  // s' value moves into the function...
    // // so is no longer valid here

    // let x = 5;             // x comes into scope
    // println!("x is valid here the value is {}", x);

    // makes_copy(x);        // x would move into function,
    // println!("x still valid, val is {}. Was copied b/c stack allocated, when went into fn", x);

    // println!("s is not valid because it was used up, {}", s); // borrow check righteous fury
    // but i32 is Copy, so it's okay to still
    // use x afterwards

    let mut s = String::from("hello");

    let r1 = &s;
    let r2 = &s;
    println!("{} and {}", r1, r2);

    println!("r2 {}", r2);
    let r3 = &mut s;
    println!("r3 {}", r3);
    println!("r2 {}", r2); // borrow checker righteous fury
}

// fn takes_ownership(some_string: String) { // some_string comes into scope
//     println!("{}", some_string);
// } // Here, some_string goes out of scope and the `drop` is called. The backing
// Memory is freed!

// fn makes_copy(some_integer: i32) {
//     println!("{}", some_integer);
// } // Here, some_integer goes out of scope. Nothing special happens


// fn main() {
//     let mut s = String::from("hello");

//     change(&mut s);
// }

// fn change(some_string: &mut String) {
//     some_string.push_str(", world");
// }

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);
 
let black = Color(0, 0, 0);
let origin = Point(0, 0, 0);
