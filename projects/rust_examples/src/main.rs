#![allow(dead_code)] // silence warnings about fields that are never read
#![allow(unused_variables)] // silence warning about variables that are never used
#![allow(unused_assignments)] // silence warnings about values being assigned to var then never used before the next assignment
struct Student {
    id: u32,
    fname: String,
    lname: String,
    gpa: f32,
}

enum GameState {
    Quit,
    Playing,
    Paused,
}

enum Key {
    Esc,
    Space,
    Q,
}
    
impl GameState {
    fn switch_state(key: Key) -> GameState {
	match key {
	    Key::Esc => {
		println!("Pausing Game");
		GameState::Paused
	    },
	    Key::Space => GameState::Playing,
	    Key::Q => GameState::Quit,
	}
    }
}

enum UsState {
    Massachusetts,
    Florida,
    Michigan,
}

// Encoding a type into an Enum
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}
    


fn main() {
    let edward = Student {
	id: 32,
	fname: String::from("edward"),
	lname: String::from("cullen"),
	gpa: 4.0};
    // just trying to test out structs vs enums
    //fn calls not allowed in match statements
    // structs aren't very useful in match statements
    // match edward.fname {
    // 	String::from("edward") => println!("It's edward"),
    // 	_ => println!("It's not edward"),
    // }

    let mut state = GameState::Playing;

    state = GameState::switch_state(Key::Q);

    let coin = Coin::Quarter(UsState::Michigan);
    let coin2 = Coin::Quarter;

    match coin {
	Coin::Quarter(state) => {
	    println!("Your quarter has a state");
	    25},
	_ => { println!("Everything Else");
	       1
	}
    };

    let null_value: i32; // so I can initialize a var to nothing, but I can't do anything with it
    let null_value = 10;

    match null_value {
	10 => println!("It is 10"),
	_ => println!("Everything else"),
}


    let null_value2: Option<i32> = None;
    println!("null_value2 is: {:?}", null_value2); // Really have to try hard to work with None values
    match null_value2 {
	None => println!("Your value is null"),
	_ => println!("Everything else"),
    }
    let null_value2 = Some(100);
    // This does not invalidate reference to null_value2. Take does 
    println!("The value of null_value2 is: {}", null_value2.expect("null_value2 is null"));

    println!("The value of null_value2 is: {}", null_value2.expect("null_value2 is null"));    

}
