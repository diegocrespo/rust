mod front_of_house {
    mod hosting {
	fn add_to_waitlist() {}
    }
}

pub fn eat_at_restaurant() {
    // Absolute path
    crate::front_of_house::hosting::add_to_waitlist();

    // Relative path
    front_of_house::hosting::add_to_waitlist();
}
// mod front_of_house {
//     mod hosting {
// 	fn add_to_waitlist() {}
// 	fn seat_at_table() {}
//     }

//     mod serving {
// 	fn take_order() {}
// 	fn serve_order() {}
// 	fn take_payment() {}
//     }
// }

// // #[cfg(test)]
// // mod tests {
// //     #[test]
// //     fn it_works() {
// //         assert_eq!(2 + 2, 4);
// //     }
// // }
