// enum IpAddrKind {
//     V4,
//     V6,
// }

// struct IpAdrr {
//     kind: IpAddrKind,
//     address: String,
// }

fn main() {
    // let four = IpAddrKind::V4;
    // let six = IpAddrKind::V6;
    // route(four);
    // route(six);

    // let home = IpAddr {
    // 	kind: IpAddrKind::V4,
    // 	address: String::from("127.0.0.1"),
    // };

    // let loopback = IpAddr {
    // 	kind: IpAddrKind::V6,
    // 	address: String::from("::1"),
    // };

    let some_number = Some(5);
    let some_stirng = Some("a string");

    let absent_number: Option<i32> = None;

    let x: i8 = 5;
    let y: Option<i8> = Some(5);

    let sum = x + y;
}

// fn route(ip_kind: IpAddrKind) {}



